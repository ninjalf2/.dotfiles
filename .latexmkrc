$latex = 'latex  %O  --shell-escape %S';
$pdflatex = 'pdflatex  %O  --shell-escape %S';
$clean_ext = "aux bbl blg dvi fdb_latexmk fls log nav out snm synctex.gz toc vrb";
$pdf_mode = 1;
