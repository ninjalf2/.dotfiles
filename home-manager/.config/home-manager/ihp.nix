{ config, pkgs, ... }:
{
  programs = {
    direnv.enable = true;
  };
  home.packages = with pkgs; [
    ihp-new
    gnumake
    cachix
    gcc
    (import (fetchTarball https://install.devenv.sh/latest)).default
  ];
}
