{ pkgs, ...}:
{
  home.packages = [
    pkgs.haskellPackages.yesod
    pkgs.haskellPackages.yesod-bin
  ];
}
