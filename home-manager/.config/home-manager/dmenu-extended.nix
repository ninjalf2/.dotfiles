{}:

let pkgs = import <nixpkgs> {};
    version = "1.2.1";
in
pkgs.python3Packages.buildPythonPackage {
  propagatedBuildInputs = with pkgs.python3Packages; [ setuptools ];
  pname = "dmenu-extended";
  version = version;
  format = "pyproject";
  src = pkgs.fetchFromGitHub {
    owner = "MarkHedleyJones";
    repo = "dmenu-extended";
    rev = version; # use version variable here because tags match
    sha256 = "sha256-IYp5S3etgiF2Js94HmQ737QjnWNbvLcA4RzvcDfycq4=";
  };
}
