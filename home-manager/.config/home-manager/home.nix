{ config, pkgs, ... }:

let
  inherit (pkgs) haskellPackages;
  haskellDeps = ps: with ps; [
    base
    lens
    newtype
  ];

  ghc = haskellPackages.ghcWithPackages haskellDeps;
  dmenu-extended = pkgs.callPackage ./dmenu-extended.nix {};
in
{
  imports = [
    #./ihp.nix
    #./yesod.nix
  ];
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "lasse";
  home.homeDirectory = "/home/lasse";

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "22.11";

  nix.settings.experimental-features = [ "nix-command" "flakes" ];
  nix.package = pkgs.nixVersions.stable;

  programs = {
    # Let Home Manager install and manage itself.
    home-manager.enable = true;

    git = {
      enable = true;
      userName = "Lasse HH";
      userEmail = "ninjalf2@gmail.com";

      extraConfig = {
        pull.rebase = "merges";

        push.default = "simple";

        branch.autosetuprebase = "always";

        init.defaultBranch = "main";

        color.ui = true;

        "color \"diff\"".whitespace = "red reverse";
      };
    };

    gpg.enable = true;

    zsh = {
      enable = true;

      shellAliases = {
        isodatetime = "date +%Y-%m-%d_%H:%M:%S";
        ls = "ls --color=auto";
        lsh = "ls -d .[^.]*";
        lshd = "ls -d .[^.]*/";
        lh = "ls -lahF --time-style=\"+%F %T\"";
        grep = "grep --color=auto";
        igrep = "grep -i";
        cp = "cp -i";
        mv = "mv -i";
        rm = "echo This is not the command you are looking for.; false";
        c = "clear";
        q = "exit";
        bell = "echo -en \"\a\"";
        reswap = "sudo swapoff -a && sudo swapon -a";
        vpn-reconnect = "sudo systemctl start openvpn-reconnect.service";
        revpn = "sudo systemctl restart openvpn-random.service";
        ipcheck = "curl icanhazip.com";
        bootmode = "[ -d /sys/firmware/efi ] && echo UEFI || echo BIOS";
        google-chrome-disposable = "sudo docker run --rm -itd --net host -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY -v /dev/snd:/dev/snd --privileged --shm-size=\"2g\" jess/chrome --incognito --no-first-run";
        firefox-disposable = "sudo docker run --rm -it --net host -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY -v /dev/snd:/dev/snd --shm-size=\"2g\" jlesage/firefox";
        firefox = "firefox --private-window";
        google-chrome = "google-chrome-stable --incognito";
        bat = "echo \"scale=2;`cat /sys/class/power_supply/BAT1/charge_now` / `cat /sys/class/power_supply/BAT1/charge_full` * 100\" | bc";
        nixos-rebuild = "echo Must be run with sudo";
        rebuild = "sudo nixos-rebuild switch";
        upgrade = "sudo nix-channel --update && sudo nixos-rebuild switch && home-manager switch";
      };

      history = {
        expireDuplicatesFirst = true;
        ignoreDups = true;

        # do not save history, keep it local to each shell
        save = 0;
        size = 10000;
        share = false;
        path = "-";
      };

      autosuggestion.enable = true;
      enableCompletion = true;
      syntaxHighlighting.enable = true;
      defaultKeymap = "emacs";

      # initExtraFirst = ''
      #   zmodload zsh/zprof
      # '';

      initExtra = ''
        bindkey "^[[1;5C" emacs-forward-word
        bindkey "^[[1;5D" emacs-backward-word

        source ~/.p10k.zsh # do this in a better way
        eval "$(direnv hook zsh)" # required for ihp haskell framework

        # zprof
      '';

      prezto = {
        enable = true;
        editor.keymap = "emacs";
        prompt.theme = "powerlevel10k";
      };
    };
    direnv.enable = true;
  };

  home.sessionVariables = {
    EDITOR = "emacs";
    VISUAL = "emacs";
    LD_PRELOAD = "${pkgs.stderred}/lib/libstderred.so";
    LESSHISTFILE = "-";
    GTK_THEME = "Materia:dark";
  };

  gtk = {
    enable = true;
    theme = {
      name = "Materia:dark";
      package = pkgs.materia-theme;
    };
  };

  # home.file."recently-used.xbel" = {
  #   target = ".local/share/recently-used.xbel";
  #   text = "";
  #   onChange = "chattr +i .local/share/recently-used.xbel";
  # };

  #programs.fuse.userAllowOther = true;
  home.packages = with pkgs; [
    # basic utilities
    wget
    netcat
    unzip
    gnutar
    gzip
    file
    bc
    pv
    htop
    dig
    jq
    util-linux # for rfkill

    # editors
    emacs29 ispell
    vim

    # development packages
    ghc
    ghcid
    hlint
    haskell-language-server
    #python # marked as insecure for some reason so let's leave it out
    python3
    pylint
    sqlite

    gnupg
    pinentry
    trash-cli

    encfs
    multipath-tools

    # SSH
    openssh
    sshfs
    autossh

    # terms & shells
    tmux
    tmate

    stow

    # media
    yt-dlp
    castnow

    stderred
    dmenu-extended
  ];

  services.syncthing = {
    enable = true;
    extraOptions = ["--gui-apikey=rymjJsRxUD5vXHpNKExeXftS9Xd9S2ZT"];
    tray = {
      enable = true;
      command = "syncthingtray --wait";
    };
  };

  # required for syncthingtray to work
  systemd.user.targets.tray = {
    Unit = {
      Before = [ "syncthingtray.target" ];
      Description = "Home Manager System Tray";
      Requires = [ "graphical-session-pre.target" ];
    };
  };
}
