(require 'package)
(setq package-enable-at-startup nil)
(setq package--init-file-ensured t)
(setq package-archives '(
                         ("elpa" . "https://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")
                         ))
(package-initialize)

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))


;; THEME
(use-package haki-theme
  :ensure t
  :config
  (setq haki-region "#2e8b6d")
  (load-theme 'haki t))


;; EDITOR / IDE
(use-package ace-jump-mode
  :ensure t
  :bind ("M-s M-s" . ace-jump-mode))

(use-package all-the-icons
  :ensure t)

(use-package browse-kill-ring
  :ensure t)

(use-package company
  :ensure t
  :init (global-company-mode t)
  (setq company-dabbrev-downcase nil))

(use-package doom-modeline
  :ensure t
  :defer t
  :init (doom-modeline-mode t))

(use-package expand-region
  :ensure t
  :bind (("M-`" . er/expand-region)
         ("M-~" . er/contract-region)))

(use-package fill-column-indicator
  :ensure t)

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode t)
  :bind ("C-f" . flycheck-mode))

(use-package goto-last-change
  :ensure t)

(use-package magit
  :ensure t
  :bind ("C-x g" . magit))

(use-package multiple-cursors
  :ensure t
  :bind ("C-x M-c" . mc/edit-lines)
        ([mouse-2] . mc/add-cursor-on-click))

(use-package neotree
  :ensure t
  :bind ("M-n" . neotree-toggle))

(use-package helm
  :ensure t
  :init (helm-mode t)
  :bind (("C-x C-f" . helm-find-files)
         ("M-x" . helm-M-x)
         ("C-x b" . helm-buffers-list))
         ("M-y" . helm-show-kill-ring))

(use-package helm-company
  :ensure t)

(use-package helm-projectile
  :ensure t
  :init (helm-projectile-on))

(use-package lsp-mode
  :init
  (setq lsp-keymap-prefix "C-c l")
  :hook ((haskell-mode . lsp)
         (haskell-literate-mode . lsp)
         (lsp-mode . lsp-enable-which-key-integration))
  :commands lsp)

(use-package lsp-haskell
  :ensure t)

(use-package lsp-ui
  :ensure t)

(use-package projectile
  :init (projectile-mode t)
        (define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
        (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
        (setq projectile-completion-system 'helm)
  :ensure t)

(use-package rainbow-delimiters
  :init (add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
  :ensure t)

(use-package which-key
  :ensure t)



;; LaTeX

(use-package auctex
  :defer t
  :ensure t)



;; MODES

(use-package flymd
 :ensure t)

(use-package haskell-mode
  :ensure t)

(use-package markdown-mode
  :ensure t)

(use-package nix-mode
  :ensure t)

(use-package rainbow-mode
  :ensure t)

(use-package yaml-mode
  :ensure t)
