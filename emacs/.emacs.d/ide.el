(setq-default custom-file
              (concat user-emacs-directory
                      (convert-standard-filename "custom.el")))

(load custom-file :noerror)


;; Looks (basic UI settings)
(setq inhibit-startup-message t)                  ; disable GNU splash screen
(menu-bar-mode -1)                                ; hide menu bar
;; (tool-bar-mode -1)                                ; hide tool bar
(when (display-graphic-p)
      (scroll-bar-mode -1))                       ; hide scroll bar
(setq-default cursor-type 'bar)
(global-display-line-numbers-mode t)


;; TeX stuff
(setq-default TeX-view-program-list '(("Evince" "evince --page-index=%(outpage) %o")))
(setq-default TeX-view-program-selection '((output-pdf "Evince")))
(add-hook 'LaTeX-mode-hook 'TeX-source-correlate-mode)
(setq-default TeX-source-correlate-start-server t)


;; Editing
(put 'upcase-region 'disabled nil) ; re-enable useful function
(put 'downcase-region 'disabled nil) ; re-enable useful function


;; Navigation
(fset 'yes-or-no-p 'y-or-n-p) ; use y and n for questions


;; Indication/feedback
;; (which-function-mode t)
(show-paren-mode t)                              ; turn on highlight paren mode
(electric-pair-mode t)
(column-number-mode t)                           ; show column numbers
(setq-default visible-bell nil)                  ; turn off bip warnings
(setq-default ring-bell-function #'ignore)
(delete-selection-mode t)                        ; delete when typing on selected text
(setq-default use-dialog-box nil)
(add-hook 'text-mode-hook 'hl-line-mode t)       ; highlight the current line for text editing
(add-hook 'text-mode-hook 'flyspell-mode)        ; enable spell checking for text files
;(add-hook 'prog-mode-hook 'flyspell-prog-mode)   ; enable spell checking in comments
(setq-default truncate-lines t)                  ; do not wrap lines
(setq-default hi-lock-highlight-range 200000000) ; increase range of highlight mode


; Enable hideshow globally so we can hide all those function bodies!
(add-hook 'prog-mode-hook #'hs-minor-mode)


;; Custom settings for built-in modes
(defun my-c-hook ()
  (setq c-basic-offset 4)
  (c-set-offset 'substatement-open 0)
  (c-set-offset 'brace-list-open 0))
(add-hook 'c-mode-hook 'my-c-hook)

(defun my-c++-hook ()
  (setq c-basic-offset 4)
  (c-set-offset 'substatement-open 0))
(add-hook 'c++-mode-hook 'my-c++-hook)

(defun my-java-hook ()
  (setq c-basic-offset 4)
  (c-set-offset 'substatement-open 0))
(add-hook 'java-mode-hook ' my-java-hook)

;; Scrolling
(setq-default mouse-wheel-scroll-amount '(1 ((shift) . 5) ((control) . nil))
              mouse-wheel-progressive-speed nil
              redisplay-dont-pause t
              scroll-margin 1
              scroll-step 1
              scroll-conservatively 10000
              scroll-preserve-screen-position 1)


;; Whitespace stuff
(setq-default indent-tabs-mode nil)    ; indent with spaces, not tabs
(setq-default tab-width 4              ; tab size set to 4 spaces
              require-final-newline t) ; require newlines at end of files

(global-whitespace-mode t)
(setq-default whitespace-line-column 80
              whitespace-style '(face tabs lines-tail trailing)
              ci-rule-column 80
              fill-column 80)


;; Buffer reverting
(global-auto-revert-mode t)
(defun revert-buffer-no-confirm ()
  "Revert buffer without confirmation."
  (interactive)
  (revert-buffer t t))


;; Backup settings
(setq-default backup-by-copying t     ; don't clobber symlinks
              kept-new-versions 10    ; keep 10 latest versions
              kept-old-versions 2     ; don't bother with old versions
              delete-old-versions t   ; don't ask about deleting old versions
              version-control t       ; number backups
              vc-make-backup-files t) ; backup version controlled files

(setq-default backup-directory-alist
              `((".*" . ,temporary-file-directory)))
(setq-default auto-save-file-name-transforms
              `((".*" ,temporary-file-directory t)))


(defun copy-whole-line (&optional arg)
  "Do a 'kill-whole-line ARG' but copy rather than kill.
This function directly calls 'kill-whole-line ARG' but in read-only mode."
  (interactive "P")
  (let ((buffer-read-only t)
        (kill-read-only-ok t))
    (kill-whole-line arg)))
