;; Keybindings
(defun kb (k) (read-kbd-macro k))
(dolist (pair
  '(("C-k"         kill-whole-line)      ; Ctrl+k kills whole line
    ("M-k"         copy-whole-line)
    ("M-c"         comment-line)         ; Alt+c comment whole line
    ("C-n"         hippie-expand)
    ("C-+"         text-scale-increase)
    ("C--"         text-scale-decrease)
    ("C-c f"       fci-mode)
    ("C-c w"       whitespace-mode)
    ("C-<return>"  open-line)
    ("M-a"         back-to-indentation)
    ("M-<left>"    windmove-left)
    ("M-<right>"   windmove-right)
    ("M-<up>"      windmove-up)
    ("M-<down>"    windmove-down)
    ))
  (global-set-key (kb (car pair)) (cadr pair)))


;; PuTTY workarounds
(global-set-key "\M-[1;5C"    'forward-word)  ; Ctrl+right   => forward word
(global-set-key "\M-[1;5D"    'backward-word) ; Ctrl+left    => backward word
