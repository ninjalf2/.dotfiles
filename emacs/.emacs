(tool-bar-mode -1)                                ; hide tool bar
(load (concat user-emacs-directory
        (convert-standard-filename "my-packages.el")))

(load (concat user-emacs-directory
        (convert-standard-filename "ide.el")))

(load (concat user-emacs-directory
        (convert-standard-filename "keybindings.el")))
