#!/usr/bin/env sh

if [ "$SOURCE" == "default" ] || [ -z "$SOURCE" ]
then
    SOURCE="$(pactl get-default-source)"
fi

case "$BLOCK_BUTTON" in
    1|3) pactl set-source-mute "$SOURCE" toggle ;;
esac

case $(pacmd list-sources | grep -A 11 "$SOURCE" | awk '/muted/ {print $2; exit}') in
    yes)
      echo ""
      echo ""
      echo "#00FF00"
      ;;
    no)
      echo ""
      echo ""
      echo "#FF0000"
      ;;
esac
