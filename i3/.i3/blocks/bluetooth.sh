#!/usr/bin/env sh

CONNECTED="#FF0000"
DISCONNECTED="#00FF00"

(rfkill list bluetooth | grep -q yes || ! hcitool dev | grep -v "Devices:" >/dev/null 2>&1) && COLOR="${DISCONNECTED}" || COLOR="${CONNECTED}"
echo "<span color=\"${COLOR}\"></span>"
