#!/usr/bin/env sh

# Thanks to zmc on StackOverflow
# https://stackoverflow.com/a/18085357 under CC BY-SA 3.0 DEED
function getGreenToRed {
    PERCENT="$1"
    if (( PERCENT > 100 ))
    then
        echo "0 255 0"
        return 0
    fi
    if (( PERCENT < 0))
    then
        echo "255 0 0"
        return 0
    fi
    R=$(( PERCENT < 50 ? 255 : (255 - (PERCENT * 2 - 100) * 255 / 100) ))
    G=$(( PERCENT > 50 ? 255 : ((PERCENT * 2) * 255 / 100) ))
    printf "$R $G 0"
}

function RGBToHEX {
    R="$1"
    G="$2"
    B="$3"
    printf "#%02X%02X%02X" $R $G $B
}

# Left click
if [[ "${BLOCK_BUTTON}" -eq 1 ]] || [[ "${BLOCK_BUTTON}" -eq 4 ]]; then
    brightnessctl s 1%+ >/dev/null
# Right click
elif [[ "${BLOCK_BUTTON}" -eq 3 ]] || [[ "${BLOCK_BUTTON}" -eq 5 ]]; then
    brightnessctl s 1%- >/dev/null
fi

CUR_BRIGHTNESS=$(brightnessctl g)
MAX_BRIGHTNESS=$(brightnessctl m)

PCT_BRIGHTNESS=$(echo "scale=4;${CUR_BRIGHTNESS} / ${MAX_BRIGHTNESS} * 100" | bc)

PCT_BRIGHTNESS="${PCT_BRIGHTNESS%.*}"

INVERSE_BRIGHTNESS="$(( 100-$PCT_BRIGHTNESS ))"

echo "${PCT_BRIGHTNESS}%"
echo "${PCT_BRIGHTNESS}%"
echo "$(RGBToHEX $(getGreenToRed ${INVERSE_BRIGHTNESS}))"
