#!/usr/bin/env bash

# Most of this script comes from the wifi and iface modules of
# https://github.com/vivien/i3blocks-contrib/tree/master/wifi

# Thanks to zmc on StackOverflow
# https://stackoverflow.com/a/18085357 under CC BY-SA 3.0 DEED
function getGreenToRed {
    PERCENT="$1"
    if (( PERCENT > 100 ))
    then
        echo "0 255 0"
        return 0
    fi
    if (( PERCENT < 0))
    then
        echo "255 0 0"
        return 0
    fi
    R=$(( PERCENT < 50 ? 255 : (255 - (PERCENT * 2 - 100) * 255 / 100) ))
    G=$(( PERCENT > 50 ? 255 : ((PERCENT * 2) * 255 / 100) ))
    printf "$R $G 0"
}

function RGBToHEX {
    R="$1"
    G="$2"
    B="$3"
    printf "#%02X%02X%02X" $R $G $B
}

function echoRGB {
    MSG="$1"
    R="$2"
    G="$3"
    B="$4"
    printf "\x1b[38;2;${R};${G};${B}m${MSG}\x1b[0m\n"
}

if [[ "$BLOCK_BUTTON" -eq 1 ]]
then
    if ( unalias rfkill; type rfkill ) >/dev/null 2>&1
    then
        rfkill toggle wlan
    else
        exit 1
    fi
fi

if [[ -z "$INTERFACE" ]]
then
    INTERFACE="${BLOCK_INSTANCE:-$(ip route | awk '/^default/ { print $5 ; exit }')}"
fi

# Exit if there is interface specified or no default route
[[ -z "$INTERFACE" ]] && exit


# If the wifi interface exists but no connection is active, "down" shall be displayed.
if [[ "$(cat /sys/class/net/$INTERFACE/operstate)" = 'down' ]]; then
    echo "$INTERFACE : down"
    echo "$INTERFACE : down"
    echo "$(RGBToHEX $(getGreenToRed 0))"
    exit
fi


AF=${ADDRESS_FAMILY:-inet6?}

for flag in "$1" "$2"; do
  case "$flag" in
    -4)
      AF=inet ;;
    -6)
      AF=inet6 ;;
    -L)
      if [[ "$IF" = "" ]]; then
        LABEL="iface"
      else
        LABEL="$IF:"
      fi ;;
  esac
done

# if no interface is found, use the first device with a global scope
IPADDR=$(ip addr show $INTERFACE | perl -n -e "/$AF ([^ \/]+).* scope global/ && print \$1 and exit")

QUALITY=$(iw dev ${INTERFACE} link | grep 'dBm$' | grep -Eoe '-[0-9]{2}' | awk '{print  ($1 > -50 ? 100 :($1 < -100 ? 0 : ($1+100)*2))}')

if [[ -d /sys/class/net/${INTERFACE}/wireless ]]
then
    SYM=""
    SSID="$(iw $INTERFACE info | grep -Po '(?<=ssid ).*' | tr -d " \t\n\r") : "
else
    SYM=""
fi

printf "${SYM}${INTERFACE} : ${SSID}${IPADDR}\n"
printf "${SYM}${INTERFACE} : ${SSID}${IPADDR}\n"
printf "$(RGBToHEX $(getGreenToRed $QUALITY))\n"
