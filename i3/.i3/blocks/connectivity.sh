#!/usr/bin/env sh

DEFAULT_GATEWAY="$(ip route | awk '/default/ { print $3 }')"

CONNECTED="#00FF00"
DISCONNECTED="#FF0000"

LOCAL_SYM=""
GLOBAL_SYM=""
DNS_SYM=""


if ping -q -w 1 google.com >/dev/null 2>&1
then
    DNS_COLOR="$CONNECTED"
    GLOBAL_COLOR="$CONNECTED"
    LOCAL_COLOR="$CONNECTED"
else
    DNS_COLOR="$DISCONNECTED"
    if ping -q -w 1 1.1.1.1 >/dev/null 2>&1
    then
        GLOBAL_COLOR="$CONNECTED"
    else
        if ping -q -w 1 "$DEFAULT_GATEWAY" >/dev/null 2>&1
        then
            LOCAL_COLOR="$CONNECTED"
        else
            LOCAL_COLOR="$DISCONNECTED"
        fi
        GLOBAL_COLOR="$DISCONNECTED"
    fi
fi

if ip l | grep tun0 | grep -q UP && [[ "$GLOBAL_COLOR" == "$CONNECTED" ]]
then
    VPN="<span color=\"${CONNECTED}\"></span>"
else
    VPN="<span color=\"${DISCONNECTED}\"></span>"
fi

LOCAL="<span color=\"${LOCAL_COLOR}\">${LOCAL_SYM}</span>"
GLOBAL="<span color=\"${GLOBAL_COLOR}\">${GLOBAL_SYM}</span>"
DNS="<span color=\"${DNS_COLOR}\">${DNS_SYM}</span>"

OUTPUT="${LOCAL} ${GLOBAL} ${DNS} ${VPN}"

echo "${OUTPUT}"
