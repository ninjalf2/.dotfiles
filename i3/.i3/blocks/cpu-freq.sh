#!/usr/bin/env sh

echo $(cpupower frequency-info | grep 'current CPU frequency' | grep asserted | awk '{print $4,$5}')
