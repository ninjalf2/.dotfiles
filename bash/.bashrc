# Don't save history
unset HISTFILE

# Bash completion
[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion

# Aliases
alias cp='cp -i'
alias mv='mv -i'
alias rm='echo This is not the command you are looking for.; false'
alias ls='ls --color=auto'
alias lsh='ls -d .[^.]*'
alias lshd='ls -d .[^.]*/'
alias lh='ls -lahF --time-style="+%F %T"'
alias igrep='grep -i'
alias grep='grep --color'
alias c='clear'
alias q='exit'
alias bell='echo -en "\a"'
alias fumount='fusermount -u'
alias ipcheck='curl icanhazip.com'
alias bootmode='[ -d /sys/firmware/efi ] && echo UEFI || echo BIOS'
alias google-chrome-disposable='sudo docker run --rm -itd --net host -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY -v /dev/snd:/dev/snd --privileged jess/chrome --incognito --no-first-run'
alias firefox-disposable='sudo docker run --rm -it --net host -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY -v /dev/snd:/dev/snd --shm-size="2g" jlesage/firefox'
alias firefox='firefox --private-window'
alias google-chrome='google-chrome-stable --incognito'
alias bat='echo "scale=4;`cat /sys/class/power_supply/BAT0/charge_now` / `cat /sys/class/power_supply/BAT0/charge_full` * 100" | bc'
alias nixos-rebuild='echo Must be run with sudo'
alias rebuild='sudo nixos-rebuild switch'
alias upgrade='sudo nix-channel --update; sudo nixos-rebuild switch'


PROMPT_COMMAND=__prompt_command # Func to gen PS1 after CMDs

__prompt_command() {
    local EXIT="$?"             # This needs to be first

    local WHITE='\[\e[0m\]'
    local BLUE='\[\e[01;34m\]'

    if [ $EXIT == 0 ]; then
        EXIT_COLOR='\[\e[01;32m\]'
    else
        EXIT_COLOR='\[\e[01;31m\]'
    fi

    if (( $UID > 0 )); then
        USER_COLOR='\[\e[01;32m\]'
    else
        USER_COLOR='\[\e[01;31m\]'
    fi

    PS1="${EXIT_COLOR}${EXIT}${WHITE}[\t]${USER_COLOR}\u@\h${WHITE}:${BLUE}\w${USER_COLOR}\$${WHITE} "
}

eval `dircolors -b`

# Disable Software Flow Control (Ctrl-S locking) as per
# http://unix.stackexchange.com/questions/72086/ctrl-s-hang-terminal-emulator
[[ $- == *i* ]] && stty -ixon
